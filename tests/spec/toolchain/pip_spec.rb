# frozen_string_literal: true

require 'spec_helper'

context 'pip packages' do
  describe package('pipenv') do
    it { should be_installed.by('pip') }

    it 'should install a Pipfile' do
      with_fixtures do |fixture_dir|
        cmd = command("cd #{fixture_dir}/pipenv && pipenv install --deploy")
        expect(cmd).to be_a_successful_cmd
        expect(cmd.stdout).to match(/Installing dependencies from Pipfile.lock/)

        cmd = command("cd #{fixture_dir}/pipenv && pipenv run http --version")
        expect(cmd).to be_a_successful_cmd
      end
    end
  end
end

