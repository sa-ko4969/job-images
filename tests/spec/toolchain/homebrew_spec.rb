# frozen_string_literal: true

require 'spec_helper'

context 'homebrew' do
  brew_repository = machine_is_arm? ? '/opt/homebrew' : '/usr/local/Homebrew'

  # Install dir for homebrew should exist
  describe file("#{brew_repository}/.git") do
    it { should be_directory }
  end

  # Check that this actually runs homebrew
  describe command('brew help') do
    it { should be_a_successful_cmd }
    its(:stdout) { should match(/brew install FORMULA/) }
  end

  describe command('printenv') do
    it { should be_a_successful_cmd }

    its(:stdout) { should_not match(/^HOMEBREW_NO_AUTO_UPDATE=1$/) }
  end

  # Taps should be tapped
  context 'taps' do
    describe 'homebrew/homebrew-core' do
      let(:subject) { file("#{brew_repository}/Library/Taps/homebrew/homebrew-core") }
      it { should be_directory }
    end

    describe 'homebrew/homebrew-cask' do
      let(:subject) { file("#{brew_repository}/Library/Taps/homebrew/homebrew-cask") }
      it { should be_directory }
    end

    describe 'gitlab/homebrew-shared-runners' do
      let(:subject) { file("#{brew_repository}/Library/Taps/gitlab/homebrew-shared-runners") }
      it { should be_directory }
    end
  end
end
